package com.jeasy.demo.blog.service;

import com.jeasy.base.service.BaseService;
import com.jeasy.demo.blog.model.Blog;

/**
 * 
 * @author taomk
 * @version 1.0
 * @since 2015/09/07 10:45
 */
public interface BlogService extends BaseService<Blog>{

}
