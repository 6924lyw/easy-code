package ${conf.basePackage}.${table.camelName}.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeasy.base.controller.BaseController;
import ${conf.basePackage}.${table.camelName}.model.${table.className};
import ${conf.basePackage}.${table.camelName}.service.${table.className}Service;

import lombok.extern.slf4j.Slf4j;

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Controller
@RequestMapping("/${table.camelName}")
public class ${table.className}Controller extends BaseController<${table.className}Service, ${table.className}> {

}