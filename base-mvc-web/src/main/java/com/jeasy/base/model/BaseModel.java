package com.jeasy.base.model;

import java.io.Serializable;
import java.util.Map;

import com.jeasy.AnnotationValidable;
import com.jeasy.collection.MapExUtils;

import lombok.Data;

/**
 * BaseModel
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
public class BaseModel implements AnnotationValidable, Serializable {

	private static final long serialVersionUID = 5409185459234711691L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 是否删除
	 */
	private Integer isDel;
	/**
	 * 创建时间
	 */
	private Long createAt;
	/**
	 * 创建者ID
	 */
	private Long createBy;
	/**
	 * 创建者名称
	 */
	private String createName;
	/**
	 * 更新时间
	 */
	private Long updateAt;
	/**
	 * 更新者ID
	 */
	private Long updateBy;
	/**
	 * 更新者名称
	 */
	private String updateName;

	/**
	 * 转换成Map<String, Object>对象
	 * @return
	 */
	public Map<String, Object> toMap() {
		return MapExUtils.toObjMap(this);
	}
}