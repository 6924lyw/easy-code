package ${conf.basePackage}.${table.camelName}.controllers;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.jeasy.base.controllers.BaseController;
import com.jeasy.base.controllers.ModelResult;
import ${conf.basePackage}.${table.camelName}.model.${table.className};
import ${conf.basePackage}.${table.camelName}.service.${table.className}Service;

import lombok.extern.slf4j.Slf4j;

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Path("${table.camelName}")
public class ${table.className}Controller extends BaseController {

	@Autowired
	private ${table.className}Service ${table.camelName}Service;

	@Get("list")
	public String list(${table.className} ${table.camelName}) {
		List<${table.className}> ${table.camelName}List = ${table.camelName}Service.find(${table.camelName});
    	return renderJson(new Gson().toJson(responseList(ModelResult.CODE_200, ModelResult.SUCCESS, ${table.camelName}List)));
    }

    @Get("page")
    public String page(${table.className} ${table.camelName}) {
    	int totalCount = ${table.camelName}Service.count(${table.camelName});
    	List<${table.className}> ${table.camelName}List = ${table.camelName}Service.page(${table.camelName}, getOffset(), getPageSize());
        return renderJson(new Gson().toJson(responsePage(ModelResult.CODE_200, ModelResult.SUCCESS, totalCount, ${table.camelName}List)));
	}

    @Post("add")
    public String add(${table.className} ${table.camelName}) {
        ${table.camelName}Service.save(${table.camelName});
        return renderJson(new Gson().toJson(responseMessage(ModelResult.CODE_200, ModelResult.SUCCESS)));
    }

    @Get("{id:[0-9]+}")
    public String show(@Param("id") long id) {
        ${table.className} ${table.camelName} = ${table.camelName}Service.getById(id);
        return renderJson(new Gson().toJson(responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, ${table.camelName})));
    }

	@Post("modify")
    public String modify(${table.className} ${table.camelName}) {
        ${table.camelName}Service.modify(${table.camelName});
        return renderJson(new Gson().toJson(responseMessage(ModelResult.CODE_200, ModelResult.SUCCESS)));
    }

	@Post("{id:[0-9]+}")
    public String remove(@Param("id") long id) {
        ${table.camelName}Service.remove(id);
        return renderJson(new Gson().toJson(responseMessage(ModelResult.CODE_200, ModelResult.SUCCESS)));
    }
}