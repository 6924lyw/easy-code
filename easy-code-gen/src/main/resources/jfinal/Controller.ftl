package ${conf.basePackage}.${table.camelName}.controller;

import ${conf.basePackage}.${table.camelName}.interceptor.${table.className}Interceptor;
import ${conf.basePackage}.${table.camelName}.model.${table.className};
import ${conf.basePackage}.${table.camelName}.validator.${table.className}Validator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

import lombok.extern.slf4j.Slf4j;

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Before(${table.className}Interceptor.class)
public class ${table.className}Controller extends Controller {

	public void index() {
		setAttr("${table.camelName}Page", ${table.className}.DAO.paginate(getParaToInt(0, 1), 10));
		render("${table.camelName}.html");
	}

	public void add() {
	}

	@Before(${table.className}Validator.class)
	public void save() {
		getModel(${table.className}.class).save();
		redirect("/${table.camelName}");
	}

	public void edit() {
		setAttr("${table.camelName}", ${table.className}.DAO.findById(getParaToInt()));
	}

	@Before(${table.className}Validator.class)
	public void update() {
		getModel(${table.className}.class).update();
		redirect("/${table.camelName}");
	}

	public void delete() {
		${table.className}.DAO.deleteById(getParaToInt());
		redirect("/${table.camelName}");
	}
}