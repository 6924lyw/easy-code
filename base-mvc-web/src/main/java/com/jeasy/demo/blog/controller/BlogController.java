package com.jeasy.demo.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeasy.base.controller.BaseController;
import com.jeasy.base.controller.ModelResult;
import com.jeasy.base.resolver.FromJson;
import com.jeasy.demo.blog.model.Blog;
import com.jeasy.demo.blog.service.BlogService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author taomk
 * @version 1.0
 * @since 2015/06/21 14:52
 */
@Slf4j
@Controller
@RequestMapping("/blog")
public class BlogController extends BaseController<BlogService, Blog> {

	@RequestMapping(value = "demo", method = RequestMethod.GET)
	@ResponseBody
	public ModelResult list(@FromJson Demo demo) {
		return responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, demo);
	}
}