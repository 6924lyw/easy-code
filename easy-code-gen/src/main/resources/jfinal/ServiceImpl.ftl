package ${conf.basePackage}.${table.camelName}.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${conf.basePackage}.${table.camelName}.dao.${table.className}DAO;
import ${conf.basePackage}.${table.camelName}.model.${table.className};
import ${conf.basePackage}.${table.camelName}.service.${table.className}Service;

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Service
public class ${table.className}ServiceImpl implements ${table.className}Service {

	@Autowired
	private ${table.className}DAO ${table.camelName}DAO;

	@Override
	public List<${table.className}> find() {
		return ${table.camelName}DAO.query();
	}

	@Override
	public ${table.className} findById(long id) {
		return ${table.camelName}DAO.queryById(id);
	}

	@Override
	public List<${table.className}> findByIds(long[] ids) {
		return ${table.camelName}DAO.queryByIds(ids);
	}

	@Override
	public List<${table.className}> find(int offset, int size) {
		return ${table.camelName}DAO.query(offset, size);
	}

	@Override
	public ${table.className} findFirst() {
		return ${table.camelName}DAO.queryFirst();
	}

	@Override
	public long save(${table.className} ${table.camelName}) {
		return ${table.camelName}DAO.insert(${table.camelName});
	}

	@Override
	public int batchSave(List<${table.className}> ${table.camelName}List) {
		return ${table.camelName}DAO.batchInsert(${table.camelName}List);
	}

	@Override
	public int modify(${table.className} ${table.camelName}) {
		return ${table.camelName}DAO.update(${table.camelName});
	}

	@Override
	public int batchModify(List<${table.className}> ${table.camelName}List) {
		return ${table.camelName}DAO.batchUpdate(${table.camelName}List);
	}

	@Override
	public int remove(long id) {
		return ${table.camelName}DAO.delete(id);
	}

	@Override
	public int batchRemove(long[] ids) {
		return ${table.camelName}DAO.batchDelete(ids);
	}
}