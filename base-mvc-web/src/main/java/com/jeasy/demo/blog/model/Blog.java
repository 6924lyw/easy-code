package com.jeasy.demo.blog.model;


import com.jeasy.base.model.BaseModel;

import lombok.Data;

/**
 * 
 * @author taomk
 * @version 1.0
 * @since 2015/09/07 10:45
 */
@Data
public class Blog extends BaseModel {

	private static final long serialVersionUID = 5409185459234711691L;

	/**
	 * 
	 */
	private String title;
	/**
	 * 
	 */
	private String content;
}