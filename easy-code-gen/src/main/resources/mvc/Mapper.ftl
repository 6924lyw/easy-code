<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${conf.basePackage}.${table.camelName}.dao.${table.className}DAO">
    <resultMap id="BaseResultMap" type="${conf.basePackage}.${table.camelName}.model.${table.className}">
	<#list table.columns as col>
		<#if col.name=="id">
        <id column="${col.name}" property="${col.camelName}" jdbcType="${col.myBastisType}"/>
		<#else>
        <result column="${col.name}" property="${col.camelName}" jdbcType="${col.myBastisType}"/>
		</#if>
	</#list>
    </resultMap>

    <sql id="base_column_list">
        ${table.selectView}
    </sql>

    <sql id="is_del_sql">
        AND is_del = 0
    </sql>

    <sql id="order_by_sql">
        ORDER BY id DESC
    </sql>

    <insert id="insert" useGeneratedKeys="true" keyProperty="id" parameterType="${conf.basePackage}.${table.camelName}.model.${table.className}">
        INSERT INTO `${table.name}` (${table.insertView})
        VALUES (
		<#list table.columns as col>
			<#if col.name!="id">
			<#if col_has_next>
		${r'#{'}${col.camelName},jdbcType=${col.myBastisType}},
			<#else>
		${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
			</#if>
			</#if>
		</#list>)
    </insert>

    <insert id="insertBatch" parameterType="java.util.List">
        INSERT INTO `${table.name}` (${table.insertView})
        VALUES
        <if test="list != null">
        <foreach collection="list" item="item" index="index" separator=",">
            (
        <#list table.columns as col>
            <#if col.name!="id">
                <#if col_has_next>
                ${r'#{item.'}${col.camelName},jdbcType=${col.myBastisType}},
                <#else>
                ${r'#{item.'}${col.camelName},jdbcType=${col.myBastisType}}
                </#if>
            </#if>
        </#list>
            )
        </foreach>
        </if>
    </insert>

    <insert id="insertSelective" useGeneratedKeys="true" keyProperty="id" parameterType="${conf.basePackage}.${table.camelName}.model.${table.className}">
        INSERT INTO `${table.name}`
        <trim prefix="(" suffix=")" suffixOverrides=",">
		<#list table.columns as col>
			<#if col.name!="id">
			<#if col_has_next>
            <if test="${col.camelName} != null">
            	${col.name},
            </if>
			<#else>
            <if test="${col.camelName} != null">
				${col.name}
            </if>
			</#if>
			</#if>
		</#list>
        </trim>
        <trim prefix="VALUES (" suffix=")" suffixOverrides=",">
		<#list table.columns as col>
			<#if col.name!="id">
			<#if col_has_next>
            <if test="${col.camelName} != null">
				${r'#{'}${col.camelName},jdbcType=${col.myBastisType}},
            </if>
			<#else>
            <if test="${col.camelName} != null">
				${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
            </if>
			</#if>
			</#if>
		</#list>
        </trim>
    </insert>

    <select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="java.lang.Long">
        SELECT
        <include refid="base_column_list"/>
        FROM `${table.name}`
        WHERE id = ${r'#{'}id,jdbcType=BIGINT}
        <include refid="is_del_sql"/>
    </select>

    <select id="selectBatchByPrimaryKey" resultMap="BaseResultMap" parameterType="java.util.List">
        SELECT
        <include refid="base_column_list"/>
        FROM `${table.name}`
        WHERE
        <if test="ids != null">
            <foreach collection="ids" index="index" item="id" open=" id IN (" separator="," close=")">
                ${r'#{'}id,jdbcType=BIGINT}
            </foreach>
        </if>
        <include refid="is_del_sql"/>
    </select>

    <update id="updateByPrimaryKeySelective" parameterType="${conf.basePackage}.${table.camelName}.model.${table.className}">
        UPDATE `${table.name}`
        <set>
		<#list table.columns as col>
			<#if col.name!="id">
			<#if col_has_next>
            <if test="${col.camelName} != null">
				${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}},
            </if>
			<#else>
            <if test="${col.camelName} != null">
				${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
            </if>
			</#if>
			</#if>
		</#list>
        </set>
        WHERE id = ${r'#{'}id,jdbcType=BIGINT}
    </update>

    <update id="updateByPrimaryKey" parameterType="${conf.basePackage}.${table.camelName}.model.${table.className}">
        UPDATE `${table.name}`
        SET
		<#list table.columns as col>
			<#if col.name!="id">
			<#if col_has_next>
			${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}},
			<#else>
			${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
			</#if>
			</#if>
		</#list>
        WHERE id = ${r'#{'}id,jdbcType=BIGINT}
    </update>

    <select id="selectByParams" resultMap="BaseResultMap" parameterType="java.util.HashMap">
        SELECT
        <include refid="base_column_list"/>
        FROM `${table.name}`
        WHERE 1=1
		<#list table.columns as col>
        <if test="${col.camelName} != null">
			AND ${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
		</if>
		</#list>
        <if test="ids != null">
            <foreach collection="ids" index="index" item="id" open=" AND id IN (" separator="," close=")">
                ${r'#{'}id,jdbcType=BIGINT}
            </foreach>
        </if>
        <include refid="is_del_sql"/>
        <include refid="order_by_sql"/>
        <if test="offset != null and size != null">
            LIMIT ${r'#{'}offset}, ${r'#{'}size}
        </if>
    </select>

    <select id="countByParams" resultType="java.lang.Integer" parameterType="java.util.HashMap">
        SELECT
        COUNT(DISTINCT id)
        FROM `${table.name}`
        WHERE 1=1
		<#list table.columns as col>
		<if test="${col.camelName} != null">
		    AND ${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
		</if>
		</#list>
        <if test="ids != null">
            <foreach collection="ids" index="index" item="id" open=" AND id IN (" separator="," close=")">
                ${r'#{'}id,jdbcType=BIGINT}
            </foreach>
        </if>
        <include refid="is_del_sql"/>
    </select>

    <select id="selectFirstByParams" resultMap="BaseResultMap" parameterType="java.util.HashMap">
        SELECT
        <include refid="base_column_list"/>
        FROM `${table.name}`
        WHERE 1=1
        <#list table.columns as col>
        <if test="${col.camelName} != null">
            AND ${col.name} = ${r'#{'}${col.camelName},jdbcType=${col.myBastisType}}
        </if>
        </#list>
        <if test="ids != null">
            <foreach collection="ids" index="index" item="id" open=" AND id IN (" separator="," close=")">
                ${r'#{'}id,jdbcType=BIGINT}
            </foreach>
        </if>
        <include refid="is_del_sql"/>
        <include refid="order_by_sql"/>
            LIMIT 1
    </select>

    <update id="deleteByPrimaryKey" parameterType="java.lang.Long">
        UPDATE `${table.name}`
        SET is_del = 1
        WHERE id = ${r'#{'}id,jdbcType=BIGINT}
    </update>

    <update id="deleteBatchByPrimaryKey" parameterType="java.util.List">
        UPDATE `${table.name}`
        SET is_del = 1
        WHERE
        <if test="ids != null">
            <foreach collection="ids" index="index" item="id" open=" id IN (" separator="," close=")">
                ${r'#{'}id,jdbcType=BIGINT}
            </foreach>
        </if>
    </update>
</mapper>