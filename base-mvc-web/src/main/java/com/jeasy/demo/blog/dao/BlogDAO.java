package com.jeasy.demo.blog.dao;

import com.jeasy.base.dao.BaseDAO;
import com.jeasy.demo.blog.model.Blog;

/**
 * 
 * @author taomk
 * @version 1.0
 * @since 2015/09/07 10:45
 */
public interface BlogDAO extends BaseDAO<Blog>{
}