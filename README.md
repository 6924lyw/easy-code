EASY-CODE 一款快速智能代码生成工具
==============================

前言
------------------------------
近几年，随着各大WEB框架的逐渐成熟，如：Struts/Spring MVC/Rose/JFinal等的框架，从零开始搭建一个WEB工程，变得开始呆板且生硬，费时又费力，在整个搭建过程中可谓很难有什么技术进展，然而还需要在不断进行调试测试。
随着Java领域的技术发展，底层/常用技术已逐渐被代码模板替代，从而进行快速迭代开发，尤其适用于创业公司。

本代码生成工具旨在为您解决一切生硬的代码，代码生成+手工修改半智能开发将是新的趋势，单表数据模型增删改查功能直接生成使用,可节省60%工作量，快速提高开发效率！！！

简介
-----------------------------------
EASY-CODE 是一款快速智能代码生成工具，面向使用Java开发的同仁们的。其关注各框架集成使用的基础代码构建过程。其代码中提供的base-xxx-web工程模板，目的就是为了使您在能够轻松地开发web程序。

EASY-CODE 希望整合各种技术的规范和开发标准，能使您摆脱犹豫，摆脱选择的困难，规避没有经验带来的开发风险。

EASY-CODE 不仅生成代码，同时还强调最佳实践，甚至包括名称规范。不仅仅只是提供技术，还会引导您应该如何使用好技术。

EASY-CODE 适用范围
-----------------------------------
EASY-CODE 可以应用在任何J2EE项目的开发中，尤其适合创业公司快速迭代开发中，其半智能手工的开发方式，可以显著提高开发效率60%以上，极大降低开发成本。

EASY-CODE 运行
-----------------------------------
1. 【重要】由于代码中使用了Lombok注解，首先在IDE中安装Lombok插件；
2. 配置数据库:修改easy-code-gen.properties,配置到自己的数据库连接；
3. 配置模板路径:修改easy-code-gen.properties中的template.path；
	1. Spring MVC + MyBatis:easy-code/easy-code-gen/src/main/resources/mvc；
	2. Rose:easy-code/easy-code-gen/src/main/resources/rose；
	3. JFinal:easy-code/easy-code-gen/src/main/resources/jfinal；
4. 配置目标路径:修改easy-code-gen.properties中的target.path；
	1. Spring MVC + MyBatis:easy-code/base-mvc-web；
	2. Rose:easy-code/base-rose-web；
	3. JFinal:easy-code/base-jfinal-web；
5. 配置基本包:修改easy-code-gen.properties中的base.package；
6. 运行com.jeasy.Main；

版本升级
-----------------------------------
- 1.1.2
	- 新增：请求参数校验框架；
- 1.1.1
	- Bug修复：将日志体系由logback改成log4j实现；
	- Bug修复：base-mvc-web list/page方法参数修改成具体实体类型；
- 1.1.0 支持Rose框架
	- 添加支持Rose框架的代码生成模板；
- 1.0.1
	- BaseController重构；
	- Controller/Service/DAO方法命名规范；
	- Controller/Service层添加日志切面，输出执行信息；
	- base-xxx-web工程模板添加logback日志支持；
	- Bug修复：Mapper.ftl insertBatch 不支持配置返回主键问题；
	- Bug修复：完善MyBatisType映射关系；

技术交流
-----------------------------------
* 	作者：陶邦仁</br>
* 	邮箱：mingkai.tao@gmail.com
*	微信号：15010244158