package ${conf.basePackage}.${table.camelName}.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

<#list table.columns as col>
<#if (col.classImport != "")>
import ${col.classImport};
</#if>
</#list>

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@SuppressWarnings("serial")
public class ${table.className} extends Model<${table.className}> {

    public static final ${table.className} DAO = new ${table.className}();

	private static final String TABLE_NAME = "${table.name}";

    public Page<${table.className}> paginate(int pageNumber, int pageSize) {
        return paginate(pageNumber, pageSize, "SELECT * ", "FROM " + TABLE_NAME + " ORDER BY id DESC");
    }

	<#list table.columns as col>
	/**
	 * ${col.comment}
	 */
	public ${col.type} get${col.className}() {
		return get("${col.name}");
	}
    public void set${col.className}(${col.type} ${col.camelName}) {
    	set("${col.name}", ${col.camelName});
    }

	</#list>
}