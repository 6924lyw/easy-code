package com.jeasy.demo.blog.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @author taomk
 * @version 1.0
 * @since 2015/09/07 15:34
 */
@Data
public class Blog implements Serializable {

	private static final long serialVersionUID = -990334519496260591L;

	/**
	 * 
	 */
	private Long id;

	/**
	 * 
	 */
	private String title;

	/**
	 * 
	 */
	private String content;

	/**
	 * 是否删除
	 */
	private Integer isDel;

	/**
	 * 创建时间
	 */
	private Long createAt;

	/**
	 * 创建者ID
	 */
	private Long createBy;

	/**
	 * 创建者名称
	 */
	private String createName;

	/**
	 * 更新时间
	 */
	private Long updateAt;

	/**
	 * 更新者ID
	 */
	private Long updateBy;

	/**
	 * 更新者名称
	 */
	private String updateName;

}