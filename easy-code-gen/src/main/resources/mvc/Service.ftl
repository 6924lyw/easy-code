package ${conf.basePackage}.${table.camelName}.service;

import com.jeasy.base.service.BaseService;
import ${conf.basePackage}.${table.camelName}.model.${table.className};

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
public interface ${table.className}Service extends BaseService<${table.className}>{

}
