package ${conf.basePackage}.${table.camelName}.model;

<#list table.columns as col>
<#if col.camelName != "id" && col.camelName != "isDel" && col.camelName != "createAt" && col.camelName != "createBy" && col.camelName != "createName" && col.camelName != "updateAt" && col.camelName != "updateBy" && col.camelName != "updateName" && col.classImport != "">
import ${col.classImport};
</#if>
</#list>

import com.jeasy.base.model.BaseModel;

import lombok.Data;

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Data
public class ${table.className} extends BaseModel {

	private static final long serialVersionUID = 5409185459234711691L;

	<#list table.columns as col>
	<#if col.camelName != "id" && col.camelName != "isDel" && col.camelName != "createAt" && col.camelName != "createBy" && col.camelName != "createName" && col.camelName != "updateAt" && col.camelName != "updateBy" && col.camelName != "updateName">
	/**
	 * ${col.comment}
	 */
	private ${col.javaType} ${col.camelName};
	</#if>
	</#list>
}