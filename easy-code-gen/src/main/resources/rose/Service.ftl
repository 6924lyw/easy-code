package ${conf.basePackage}.${table.camelName}.service;

import java.util.List;

import ${conf.basePackage}.${table.camelName}.model.${table.className};

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
public interface ${table.className}Service {

	/**
	 * 查询
	 */
	public List<${table.className}> find(${table.className} ${table.camelName});

    /**
     * ID查询
     */
    public ${table.className} getById(long id);

    /**
     * ID批量查询
     */
    public List<${table.className}> findByIds(List<Long> ids);

    /**
     * 参数分页查询
     */
    public List<${table.className}> page(${table.className} ${table.camelName}, int offset, int size);

    /**
     * 参数查询总数
     */
    public int count(${table.className} ${table.camelName});

    /**
     * First查询
     */
    public ${table.className} getFirst(${table.className} ${table.camelName});

    /**
     * 保存
     */
    public long save(${table.className} ${table.camelName});

    /**
      * 批量保存
      */
	public int saveBatch(List<${table.className}> ${table.camelName}List);

	/**
	 * 选择保存
	 */
	public long saveSelective(${table.className} ${table.camelName});

    /**
     * 修改
     */
    public int modify(${table.className} ${table.camelName});

    /**
     * 选择修改
     */
    public int modifySelective(${table.className} ${table.camelName});

    /**
     * 删除
     */
    public int remove(long id);

    /**
     * 批量删除
     */
    public int removeBatch(List<Long> ids);
}
