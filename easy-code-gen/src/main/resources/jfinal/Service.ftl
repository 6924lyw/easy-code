package ${conf.basePackage}.${table.camelName}.service;

import java.util.List;

import ${conf.basePackage}.${table.camelName}.model.${table.className};

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
public interface ${table.className}Service {

	/**
	 * 查询
	 */
	public List<${table.className}> find();

	/**
     * ID查询
     */
	public ${table.className} findById(long id);

	/**
	 * ID批量查询
	 */
	public List<${table.className}> findByIds(long[] ids);

	/**
	 * 分页查询
	 */
	public List<${table.className}> find(int offset, int size);

	/**
	 * First查询
	 */
	public ${table.className} findFirst();

	/**
	 * 保存
	 */
    public long save(${table.className} ${table.camelName});

	/**
	 * 批量保存
	 */
	public int batchSave(List<${table.className}> ${table.camelName}List);

	/**
	 * 修改
	 */
	public int modify(${table.className} ${table.camelName});

	/**
	 * 批量修改
	 */
	public int batchModify(List<${table.className}> ${table.camelName}List);

	/**
	 * 删除
	 */
    public int remove(long id);

	/**
	 * 批量删除
	 */
	public int batchRemove(long[] ids);
}
