package ${conf.basePackage}.${table.camelName}.dao;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;

import java.util.List;

import ${conf.basePackage}.${table.camelName}.model.${table.className};

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@DAO
public interface ${table.className}DAO {

	public static final String INSERT_VIEW = "${table.insertView}";

	public static final String SELECT_VIEW = "${table.selectView}";

	/**
     * 插入
     */
    @ReturnGeneratedKeys
    @SQL("INSERT INTO ${table.name} (" + INSERT_VIEW + ") VALUES (${table.insertValue})")
    public long insert(${table.className} ${table.camelName});

	/**
	 * 批量插入
	 */
	@SQL("INSERT INTO ${table.name} (" + INSERT_VIEW + ") VALUES (${table.insertValue})")
	public int batchInsert(List<${table.className}> ${table.camelName}List);

	/**
     * 更新
     */
	@SQL("UPDATE ${table.name} SET ${table.updateValue} WHERE id = :1.id")
	public int update(${table.className} ${table.camelName});

	/**
	 * 批量更新
	 */
	@SQL("UPDATE ${table.name} SET ${table.updateValue} WHERE id IN (:1.id)")
	public int batchUpdate(List<${table.className}> ${table.camelName}List);

	/**
	 * 删除
	 */
	@SQL("DELETE FROM ${table.name} WHERE id = :1")
	public int delete(long id);

	/**
	 * 批量删除
	 */
	@SQL("DELETE FROM ${table.name} WHERE id IN (:1)")
	public int batchDelete(long[] ids);

	/**
	 * 查询
	 */
	@SQL("SELECT " + SELECT_VIEW + " FROM ${table.name} ORDER BY id DESC")
	public List<${table.className}> query();

	/**
	 * 分页查询
	 */
	@SQL("SELECT " + SELECT_VIEW + " FROM ${table.name} ORDER BY id DESC LIMIT :1, :2")
	public List<${table.className}> query(int offset, int size);

	/**
	 * ID查询
	 */
	@SQL("SELECT " + SELECT_VIEW + " FROM ${table.name} WHERE id = :1 ORDER BY id DESC")
	public ${table.className} queryById(long id);

	/**
	 * ID批量查询
	 */
	@SQL("SELECT " + SELECT_VIEW + " FROM ${table.name} WHERE id IN (:1) ORDER BY id DESC")
	public List<${table.className}> queryByIds(long[] ids);

	/**
	 * First查询
	 */
	@SQL("SELECT " + SELECT_VIEW + " FROM ${table.name} ORDER BY id DESC LIMIT 1")
	public ${table.className} queryFirst();
}