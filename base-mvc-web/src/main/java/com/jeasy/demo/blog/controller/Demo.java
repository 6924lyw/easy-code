package com.jeasy.demo.blog.controller;

import java.util.List;

import com.jeasy.AnnotationValidable;
import com.jeasy.validate.ValidateLong;
import com.jeasy.validate.ValidateNotEmpty;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 15-8-9 上午11:27
 */
@Data
public class Demo implements AnnotationValidable {

	@ValidateLong(min=1)
	private Long id;

	private String name;

	@ValidateNotEmpty
	private List<String> scores;

	@ValidateNotEmpty
	private List<Demo> demos;
}
