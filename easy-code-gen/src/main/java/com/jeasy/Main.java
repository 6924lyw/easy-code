package com.jeasy;

import java.io.File;
import java.text.ParseException;
import java.util.Map;

import com.google.common.collect.Maps;
import com.jeasy.conf.Config;
import com.jeasy.db.DBInfo;
import com.jeasy.db.TableInfo;
import com.jeasy.util.StringExUtils;
import com.jeasy.util.TemplateUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class Main {

	public static void main(String [] args) throws ParseException {

		Config conf = new Config();
		DBInfo dbInfo = DBInfo.getInstance(conf);
		String targetJavaPath = conf.getTargetPath() + "/src/main/java" + File.separator + StringExUtils.replace(conf.getBasePackage(), ".", File.separator);
		String targetResourcesPath = conf.getTargetPath() + "/src/main/resources";

//		genCodeForMvc(conf, dbInfo, targetJavaPath, targetResourcesPath);
		genCodeForRose(conf, dbInfo, targetJavaPath, targetResourcesPath);
	}

	private static void genCodeForMvc(Config conf, DBInfo dbInfo, String targetJavaPath, String targetResourcesPath) {
		for (TableInfo table : dbInfo.getTables()) {
			Map<String, Object> model = Maps.newHashMap();
			model.put("table", table);
			model.put("conf", conf);

			// Model
			String targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "model";
			String targetName = table.getClassName() + ".java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Model.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Model : " + table.getName() + " ==> " + table.getClassName() + " success");
			System.out.println("Generate Model : " + table.getName() + " ==> " + table.getClassName() + " success");

			// DAO
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "dao";
			targetName = table.getClassName() + "DAO.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Dao.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate DAO : " + table.getName() + " ==> " + table.getClassName() + "DAO success");
			System.out.println("Generate DAO : " + table.getName() + " ==> " + table.getClassName() + "DAO success");

			// Mapper
			targetPath = targetResourcesPath + "/sqlMapper";
			targetName = table.getClassName() + "Mapper.xml";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Mapper.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Mapper : " + table.getName() + " ==> " + table.getClassName() + "Mapper success");
			System.out.println("Generate Mapper : " + table.getName() + " ==> " + table.getClassName() + "Mapper success");

			// Service
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "service";
			targetName = table.getClassName() + "Service.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Service.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Service : " + table.getName() + " ==> " + table.getClassName() + "Service success");
			System.out.println("Generate Service : " + table.getName() + " ==> " + table.getClassName() + "Service success");

			// ServiceImpl
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "service" + File.separator + "impl";
			targetName = table.getClassName() + "ServiceImpl.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "ServiceImpl.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate ServiceImpl : " + table.getName() + " ==> " + table.getClassName() + "ServiceImpl success");
			System.out.println("Generate ServiceImpl : " + table.getName() + " ==> " + table.getClassName() + "ServiceImpl success");

			// Controller
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "controller";
			targetName = table.getClassName() + "Controller.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Controller.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Controller : " + table.getName() + " ==> " + table.getClassName() + "Controller success");
			System.out.println("Generate Controller : " + table.getName() + " ==> " + table.getClassName() + "Controller success");
		}
	}

	private static void genCodeForRose(Config conf, DBInfo dbInfo, String targetJavaPath, String targetResourcesPath) {
		for (TableInfo table : dbInfo.getTables()) {
			Map<String, Object> model = Maps.newHashMap();
			model.put("table", table);
			model.put("conf", conf);

			// Model
			String targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "model";
			String targetName = table.getClassName() + ".java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Model.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Model : " + table.getName() + " ==> " + table.getClassName() + " success");
			System.out.println("Generate Model : " + table.getName() + " ==> " + table.getClassName() + " success");

			// DAO
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "dao";
			targetName = table.getClassName() + "DAO.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Dao.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate DAO : " + table.getName() + " ==> " + table.getClassName() + "DAO success");
			System.out.println("Generate DAO : " + table.getName() + " ==> " + table.getClassName() + "DAO success");

			// Service
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "service";
			targetName = table.getClassName() + "Service.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Service.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Service : " + table.getName() + " ==> " + table.getClassName() + "Service success");
			System.out.println("Generate Service : " + table.getName() + " ==> " + table.getClassName() + "Service success");

			// ServiceImpl
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "service" + File.separator + "impl";
			targetName = table.getClassName() + "ServiceImpl.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "ServiceImpl.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate ServiceImpl : " + table.getName() + " ==> " + table.getClassName() + "ServiceImpl success");
			System.out.println("Generate ServiceImpl : " + table.getName() + " ==> " + table.getClassName() + "ServiceImpl success");

			// Controller
			targetPath = targetJavaPath + File.separator + table.getCamelName() + File.separator + "controllers";
			targetName = table.getClassName() + "Controller.java";
			TemplateUtils.executeFreemarker(conf.getTemplatePath(), "Controller.ftl", "UTF-8", model, targetPath, targetName);
			log.info("Generate Controller : " + table.getName() + " ==> " + table.getClassName() + "Controller success");
			System.out.println("Generate Controller : " + table.getName() + " ==> " + table.getClassName() + "Controller success");
		}
	}
}